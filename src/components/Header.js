import React, {Component} from 'react';
import {Link} from "react-router-dom";
import '../style/Header.less';

class Header extends Component {
  render() {
    return (
      <div className={'Header'}>
        <Link to="/mall">商城</Link>
        <Link to="/mall/products">+添加商品</Link>
        <Link to="/mall/orders">订单</Link>
      </div>
    );
  }
}

export default Header;