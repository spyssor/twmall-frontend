const initState = {
  flag: false,
  productName: '',
  price: '',
  unit: '',
  imageUrl: ''
};

export default (state=initState, action)  => {

  switch (action.type) {
    case 'POST_PRODUCT':
      return {
        ...state,
        flag: action.flag,
      };
    case 'CHANGE_PRODUCT_NAME':
      return {
        ...state,
        productName: action.productName
      };
    case 'CHANGE_PRICE':
      return {
        ...state,
        price: action.price
      };
    case 'CHANGE_UNIT':
      return {
        ...state,
        unit: action.unit
      };
    case 'CHANGE_IMAGE_URL':
      return {
        ...state,
        imageUrl: action.imageUrl
      };
    default:
      return state;
  }
}