import {combineReducers} from "redux";
import createProductReducer from "./createProductReducer";
import getProductReducer from "./getProductReducer";
import getOrdersReducer from "./getOrdersReducer";

const reducers = combineReducers({
  createProductReducer: createProductReducer,
  getProductReducer: getProductReducer,
  getOrdersReducer: getOrdersReducer
});
export default reducers;