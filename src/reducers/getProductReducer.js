const initState = {
 productList: []
};

export default (state=initState, action) => {
  switch (action.type) {
    case 'GET_PRODUCTS':
      return {
        ...state,
        productList: action.productList,
      };
    default:
      return state;
  }
}