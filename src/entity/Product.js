class Product{
  constructor(name, price, unit, imageUrl) {
    this.name = name;
    this.price = price;
    this.unit = unit;
    this.imageUrl = imageUrl;
  }
}

export default Product;