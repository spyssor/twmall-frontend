import React, {Component} from 'react';
import './style/App.less';
import {BrowserRouter as Router} from "react-router-dom";
import {Route, Switch} from "react-router";
import CreateProductPage from "./product/pages/CreateProductPage";
import Mall from "./product/pages/Mall";
import Order from "./product/pages/Order";
import Header from "./components/Header";
import Footer from "./components/Footer";

class App extends Component {
  render() {
    return (
      <Router>
        <Header/>
        <main className={'main'}>
          <Switch>
            {/* Todo: the route definition is not good */}
            <Route exact path="/mall/products" component={CreateProductPage}/>
            <Route exact path="/mall/orders" component={Order}/>
            <Route exact path="/mall" component={Mall}/>
          </Switch>
        </main>
        <Footer/>
      </Router>
    );
  }
}

export default App;