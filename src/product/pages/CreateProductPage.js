import React, {Component} from 'react';
import {changeImageUrl, changePrice, changeProductName, changeUnit, createProduct} from "../actions/createProduct";
import {connect} from "react-redux";
import Product from "../../entity/Product";
import '../../style/Product.less'


class CreateProductPage extends Component {


  constructor(props) {
    super(props);
    this.handleAddProduct = this.handleAddProduct.bind(this);
    this.handleChangeUnit = this.handleChangeUnit.bind(this);
    this.handleChangeImageUrl = this.handleChangeImageUrl.bind(this);
    this.handleChangePrice = this.handleChangePrice.bind(this);
    this.handleChangeProductName = this.handleChangeProductName.bind(this);
  }

  handleAddProduct() {
    let product = new Product(this.props.productName, this.props.price, this.props.unit, this.props.imageUrl);
    this.props.createProduct(product, this.props);
  }

  handleChangeProductName(e) {
    this.props.changeProductName(e.target.value);
  }

  handleChangePrice(e) {
    this.props.changePrice(e.target.value);
  }

  handleChangeUnit(e) {
    this.props.changeUnit(e.target.value);

  }

  handleChangeImageUrl(e) {
    this.props.changeImageUrl(e.target.value);

  }

  render() {
    return (
      <div>
        <form id="addProduct">
          // todo: input should be inside label
          <label htmlFor="name">商品名称:</label>
          <input name="name" type="text" value={this.props.productName} onChange={this.handleChangeProductName} required/>
          <label htmlFor="price">价格:</label>
          <input name="price" type="number" value={this.props.price} onChange={this.handleChangePrice} required/>
          <label htmlFor="unit">单位:</label>
          <input name="unit" type="text" value={this.props.unit} onChange={this.handleChangeUnit} required/>
          <label htmlFor="image">图片地址:</label>
          <input name="imageUrl" type="text" value={this.props.imageUrl} onChange={this.handleChangeImageUrl} required/>
        </form>
        <button onClick={this.handleAddProduct}>提交</button>
      </div>
    );
  }

}

// todo: too many state
const mapStateToProps = state => ({
  flag: state.createProductReducer.flag,
  productName: state.createProductReducer.productName,
  price: state.createProductReducer.price,
  unit: state.createProductReducer.unit,
  imageUrl: state.createProductReducer.imageUrl,
  msg: state.createProductReducer.msg
  });

// todo: too many dispatch
const mapDispatchToProps = dispatch => ({
  createProduct: (product, props) => dispatch(createProduct(product, props)),
  changeProductName: (productName) => dispatch(changeProductName(productName)),
  changePrice: (price) => dispatch(changePrice(price)),
  changeUnit: (unit) => dispatch(changeUnit(unit)),
  changeImageUrl: (imageUrl) => dispatch(changeImageUrl(imageUrl))
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateProductPage);