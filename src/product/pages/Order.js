import React, {Component} from 'react';
import {getOrders} from "../actions/getOrders";
import {connect} from "react-redux";

class Order extends Component {


  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.getOrders();
  }

  handleRemove(orderId) {
    fetch(`http://localhost:8080/api/orders/${orderId}`, {
      method: 'DELETE',
    }).then(data => {
        console.log('action');
        if (data.status === 200) {
          alert("删除成功");
          window.location.reload();
        }else{
          alert('删除失败');
        }
      }
    )
  }

  render() {
    return (
      <table id="orderList">
        <tbody>
        <tr>
          <th>名字</th>
          <th>单价</th>
          <th>数量</th>
          <th>单位</th>
          <th>操作</th>
        </tr>
        {
          this.props.orders.map(order => {
          return (
            <tr key={order.name}>
              <td>{order.name}</td>
              <td>{order.price}</td>
              <td>{order.count}</td>
              <td>{order.unit}</td>
              <td><button onClick={this.handleRemove.bind(this, order.id)}>删除</button></td>
            </tr>
          )
        })
        }
        </tbody>
      </table>
    );
  }
}

const mapStateToProps = state => ({
  orders: state.getOrdersReducer.orders
});

const mapDispatchToProps = {
  getOrders
};

export default connect(mapStateToProps, mapDispatchToProps)(Order);