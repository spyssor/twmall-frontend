import React, {Component} from 'react';
import {getProducts} from "../actions/getProducts";
import {connect} from "react-redux";
import '../../style/Mall.less'

class Mall extends Component {

  constructor(props) {
    super(props);
    this.handleAddOrder = this.handleAddOrder.bind(this);
  }

  componentDidMount() {
    this.props.getProducts();
  }

  handleAddOrder(product) {
    const {name, price, unit, imageUrl} = product;
    console.log(name);

    // todo: action should not be in pages
    fetch('http://localhost:8080/api/orders', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ name:name, price:price, unit:unit, imageUrl:imageUrl})
    }).then(data => {
        if (data.status === 201) {
          alert("添加成功");
        }else{
          alert("添加失败");
        }
      }
    )
  }

  render() {
    const products = this.props.productList;
    return (
      <div className={'product-list'}>
        {products.map((product) => {
          return (<div className={'product'} key={product.id}>
            <div className={'image'}>
            <img src={product.imageUrl}/>
            </div>
            <h2>{product.name}</h2>
            <h4>单价:{product.price}元/{product.unit}</h4>
            <button className={'add-button'} onClick={() => this.handleAddOrder(product)} value={product}>+</button>
          </div>);
        })}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  productList: state.getProductReducer.productList
});

const mapDispatchToProps = {
  getProducts
};

export default connect(mapStateToProps, mapDispatchToProps)(Mall);