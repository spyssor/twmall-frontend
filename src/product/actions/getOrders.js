export const getOrders = () => (dispatch) => {
  fetch("http://localhost:8080/api/orders")
    .then(response => response.json())
    .then(data => {
      console.log(data);
      dispatch({
        type: 'GET_ORDERS',
        orders: data
      })
    })
    .catch(error => error.message);
};