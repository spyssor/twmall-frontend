export const getProducts = () => (dispatch) => {

  fetch(`http://localhost:8090/api/products`)
    .then(response => response.json())
    .then(data => {
      console.log(data);
      dispatch({
        type: 'GET_PRODUCTS',
        productList: data
      });
    });
};