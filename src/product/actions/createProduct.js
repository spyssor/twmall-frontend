export const createProduct = (product, props) => (dispatch) => {
  // todo: dispatch is not used
  // todo: no console log
  console.log(product);
  const {name, price, unit, imageUrl} = product;
  fetch('http://localhost:8080/api/products', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({name:name, price:price, unit:unit, imageUrl:imageUrl})
  }).then(data => {
    console.log('action');
    // todo: code smell for if else
      if (data.status === 201) {
        alert("创建成功");
        props.history.push('/mall');
      } else if(data.status === 1) {
        // todo: what does 1 meaning for?
        alert(data.msg);
        props.history.push('/mall');
      }else {
        // todo: error should be in catch
        alert("创建失败");
        props.history.push('/mall');
      }
    }
  )
};

// todo: change name, price, unit, image should not be separate
export const changeProductName = (productName) => (dispatch) => {
  dispatch({
    type: 'CHANGE_PRODUCT_NAME',
    productName: productName,
  });
};

export const changePrice = (price) => (dispatch) => {
  dispatch({
    type: 'CHANGE_PRICE',
    price: price
  });
};

export const changeUnit = (unit) => (dispatch) => {
  dispatch({
    type: 'CHANGE_UNIT',
    unit: unit
  });
};

export const changeImageUrl = (imageUrl) => (dispatch) => {
  dispatch({
    type: 'CHANGE_IMAGE_URL',
    imageUrl: imageUrl
  });
};